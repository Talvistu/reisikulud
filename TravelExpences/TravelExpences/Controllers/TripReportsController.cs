﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TravelExpences.Data;
using TravelExpences.Models;
using System.IO;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Data.SqlClient;

namespace TravelExpences.Controllers
{
    public class TripReportsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TripReportsController(ApplicationDbContext context)
        {
            _context = context;
        }

        //GET: TripReports
        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.TripReport.ToListAsync());
        //}

        //    }
        //    return View(await tripreport.AsNoTracking().ToListAsync());
        //}



        public IActionResult Index(string searchString)
        {
            ClaimsPrincipal currentUser = User;

            if (User.Identity.IsAuthenticated)
            {

                //var days = _context.TripReport.ToList(); // proov


                //connection
                string connectionString = @"Data Source=myawesomedatabase.database.windows.net,1433;Initial Catalog=TravelExpencesSQL;Persist Security Info=False;
                        User ID=mylogin;Password=Valiit123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                // kalendri paevadearv arvutus otse andmebaasist
                string sqlExpression = "SELECT  EndDate,StartDate FROM TripReport " +
                    "update TripReport set DaySum = datediff(day, TripReport.StartDate, TripReport.EndDate)";

                //arvutab hyvitust vajavad raha kalendripäeva arvust *32
                string sqlExpressionone = "update TripReport SET TripDuration = DaySum * 32";


                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    var Nice = command.ExecuteNonQuery();

                    SqlCommand commandone = new SqlCommand(sqlExpressionone, connection);
                    var Niceone = commandone.ExecuteNonQuery();

                    //  connection.Close();
                }


                List<TripReport> TripReports = new List<TripReport>();

                if (User.IsInRole("Tehnik") || User.IsInRole("Näitleja"))
                {

                    var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
                    TripReports = _context.TripReport
                        .Include(x => x.TripStatus)
                        .Include(x => x.User)   
                        .Where(x => Convert.ToString(x.UserId) == currentUserId)
                        .OrderByDescending(x => x.EndDate).Take(20).ToList();
       
                }

                else if (User.IsInRole("Raamatupidaja"))
                {

                    TripReports = _context.TripReport.Include(x => x.TripStatus).Include(x => x.User).Where(x => x.Id != 0).OrderByDescending(x => x.EndDate).Take(20).ToList();

                }

                else if (User.IsInRole("Haldusjuht"))
                {  

                    TripReports = _context.TripReport.Include(x => x.TripStatus).Include(x => x.User).Where(x => x.User.RoleName == "Tehnik").OrderByDescending(x => x.EndDate).Take(20).ToList();
                
                }
                else if (User.IsInRole("Näitejuht"))
                {

                    TripReports = _context.TripReport.Include(x => x.TripStatus).Include(x => x.User).Where(x => x.User.RoleName == "Näitleja").OrderByDescending(x=>x.EndDate).Take(20).ToList();
                   
                }
                else
                {
                    return RedirectToAction("Login", "Identity/Account");
                }

                if(!String.IsNullOrEmpty(searchString))
                {
                    ViewData["CurrentFilter"] = searchString;


                    TripReports = TripReports.Where(s => s.User.FullName != null && s.User.FullName.Contains(searchString) ||
                    s.StartDate.ToString().Contains(searchString)).ToList();

                }

                return View(TripReports);

            }

            else
            {
                return RedirectToAction("Login", "Identity/Account");
            } 
        }



        // GET: TripReports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                //.Include(x => x.Comments)
                .Include(x => x.TripStatus)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tripReport == null)
            {
                return NotFound();
            }

         

            return View(tripReport);

        }

        

        // GET: TripReports/Create
        public IActionResult Create()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var user = _context.Users.Where(x => x.Id == userId).FirstOrDefault();
            ViewData["FullName"] = user.FirstName + " " + user.LastName;

            return View();
        }

        // POST: TripReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserName,TripReason,Destination,StartDate,EndDate,TripDuration,TransportExpenses,AccomodationExpenses,AccomodationNight,OtherExpenses,ReadyToAccept")]
        TripReport tripReport, IFormFile file)
        {

            if (file != null)
            {
                //FileStream stream = new FileStream("Picture/" + tripReport.Id + ".jpg", FileMode.Create, FileAccess.Write);
                //picture.CopyTo(stream);
                //stream.Close();

                MemoryStream stream = new MemoryStream();
                file.CopyTo(stream);

                tripReport.File = stream.ToArray();
                tripReport.FileName = file.FileName;
                tripReport.FileType = file.ContentType;
                stream.Close();

            }

            if (ModelState.IsValid)
            {
                if(User.Identity.IsAuthenticated)
                {
                    tripReport.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                }

                TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Avatud").FirstOrDefault();

                tripReport.TripStatusId = tripStatus.Id;


                _context.Add(tripReport);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tripReport);
        }


        public IActionResult GetImageFile(int tripreportId)
        {
            var tripreport = _context.TripReport.Find(tripreportId);
            if (tripreport.File == null || tripreport.FileType == null)
            {
                return Content("Pilti pole");
            }
            return File(tripreport.File, tripreport.FileType);
        }





        //public async Task<IActionResult> Submit (int? id)
        //    {
        //        if (id == null)
        //        {
        //            return NotFound();
        //}
        //{
        //    TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Avatud").FirstOrDefault();

        //    tripReport.TripStatusId = tripStatus.Id;

        //        _context.Add(tripReport);
        //}



        public async Task<IActionResult> Submit(int? id)
        {

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);



            return View(tripReport);
        }


        [HttpPost, ActionName("Submit")]
        public async Task<IActionResult> ConfirmSubmit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData["FullName"] = tripReport.User.FirstName + " " + tripReport.User.LastName;

            TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Esitatud").FirstOrDefault();

            tripReport.TripStatusId = tripStatus.Id;

            _context.Update(tripReport);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }


        public async Task<IActionResult> Confirm(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);



            return View(tripReport);
        }


        [HttpPost, ActionName("Confirm")]
        public async Task<IActionResult> ConfirmConfirm(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData["FullName"] = tripReport.User.FirstName + " " + tripReport.User.LastName;

            TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Kinnitatud").FirstOrDefault();

            tripReport.TripStatusId = tripStatus.Id;

            _context.Update(tripReport);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }


        public async Task<IActionResult> ReturnToWorker(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);


            return View(tripReport);
        }


        [HttpPost, ActionName("ReturnToWorker")]
        public async Task<IActionResult> ConfirmReturnToWorker(int? id)
        {

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData["FullName"] = tripReport.User.FirstName + " " + tripReport.User.LastName;



            TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Avatud").FirstOrDefault();

            tripReport.TripStatusId = tripStatus.Id;

            _context.Update(tripReport);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }


        public async Task<IActionResult> Pay(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tripReport = await _context.TripReport
                .Include(x => x.User) 
 
                .FirstOrDefaultAsync(m => m.Id == id);

            return View(tripReport);
        }


        [HttpPost, ActionName("Pay")]
        public async Task<IActionResult> ConfirmPay(int? id, string comment)
        {

            var tripReport = await _context.TripReport
                .Include(x => x.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData["FullName"] = tripReport.User.FirstName + " " + tripReport.User.LastName;

            TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Välja makstud").FirstOrDefault();

            tripReport.TripStatusId = tripStatus.Id;
            tripReport.Comments = comment;

            _context.Update(tripReport);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");

           
            
           
        }

        //public async Task<IActionResult> ReturnToManager(int? id)
        //{

        //    var tripReport = await _context.TripReport
        //        .FirstOrDefaultAsync(m => m.Id == id);



        //    return View(tripReport);
        //}


        //[HttpPost, ActionName("ReturnToManager")]
        //public async Task<IActionResult> ConfirmReturnToManager(int? id)
        //{

        //    var tripReport = await _context.TripReport
        //        .FirstOrDefaultAsync(m => m.Id == id);


        //    TripStatus tripStatus = _context.TripStatus.Where(x => x.Name == "Esitatud").FirstOrDefault();

        //    tripReport.TripStatusId = tripStatus.Id;

        //    _context.Update(tripReport);
        //    _context.SaveChanges();

        //    return RedirectToAction("Index", "Home");
        //}



        public IActionResult EditErrorMessage()
        {

            return View();
        }



        // GET: TripReports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var tripReport = await _context.TripReport.Include(x => x.User).Include(x => x.TripStatus).Where(x => x.Id == id).FirstOrDefaultAsync();

            if (tripReport.TripStatus.Name == "Avatud")
            {
                if (id == null)
                {
                    return NotFound();
                }        

                if (tripReport == null)
                {
                    return NotFound();
                }
                ViewData["FullName"] = tripReport.User.FirstName + " " + tripReport.User.LastName;

                return View(tripReport);
            }
            else
            {
                return RedirectToAction(nameof(EditErrorMessage));
            }

        }




        // POST: TripReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserName,TripReason,Destination,StartDate,EndDate,TransportExpenses,AccomodationExpenses,AccomodationNight,OtherExpenses")]
        TripReport tripReport, IFormFile file)
        {


                if (file != null)
                {
                    //FileStream stream = new FileStream("Picture/" + tripReport.Id + ".jpg", FileMode.Create, FileAccess.Write);
                    //picture.CopyTo(stream);
                    //stream.Close();

                    MemoryStream stream = new MemoryStream();
                    file.CopyTo(stream);

                    tripReport.File = stream.ToArray();
                    tripReport.FileName = file.FileName;
                    tripReport.FileType = file.ContentType;
                    stream.Close();


                }

                if (id != tripReport.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        tripReport.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    }

                    try
                    {
                        _context.Update(tripReport);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!TripReportExists(tripReport.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(tripReport);
            

        }

        public IActionResult DeleteErrorMessage()
        {

            return View();
        }

        // GET: TripReports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var tripReport = await _context.TripReport.Include(x => x.User).Include(x => x.TripStatus).Where(x => x.Id == id).FirstOrDefaultAsync();

            if (tripReport.TripStatus.Name == "Avatud")
            {
                
                if (id == null)
                {
                    return NotFound();
                }

                if (tripReport == null)
                {
                    return NotFound();
                }

                return View(tripReport);
            }
            else
            {
                    return RedirectToAction(nameof(DeleteErrorMessage));
            }
        }

        // POST: TripReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tripReport = await _context.TripReport.FindAsync(id);
            _context.TripReport.Remove(tripReport);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TripReportExists(int id)
        {
            return _context.TripReport.Any(e => e.Id == id);
        }
    }
}
