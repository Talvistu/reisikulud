﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TravelExpences.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity.UI.Services;
using TravelExpences.Services;
using TravelExpences.Models;

namespace TravelExpences
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options => options.LoginPath = "/Identity/Account/LogIn");

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            CreateRoles(serviceProvider, context).Wait();
        }

        private async Task CreateRoles(IServiceProvider serviceProvider, ApplicationDbContext context)
        {
            var tripStatus = context.TripStatus.FirstOrDefault();
            if (tripStatus == null)
            {
                var statuses = new List<TripStatus>()
                {
                    new TripStatus()
                    {
                        Name = "Avatud"
                    },
                    new TripStatus()
                    {
                        Name = "Esitatud"
                    },
                    new TripStatus()
                    {
                        Name = "Kinnitatud"
                    },
                    new TripStatus()
                    {
                        Name = "Välja makstud"
                    },
                };

                context.AddRange(statuses);
                context.SaveChanges();
            }

            //initializing custom roles   
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = { "Raamatupidaja", "Näitleja", "Tehnik", "Haldusjuht", "Näitejuht" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1  
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            ApplicationUser user = await UserManager.FindByEmailAsync("raamatupidaja@valiteater.ee");

            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = "raamatupidaja@valiteater.ee",
                    Email = "raamatupidaja@valiteater.ee",
                    FirstName = "Malle",
                    LastName = "Maasikas",
                    RoleName = "Raamatupidaja"

                };
                await UserManager.CreateAsync(user, "Test!123");
                await UserManager.AddToRoleAsync(user, "Raamatupidaja");
            }








            ApplicationUser user5 = await UserManager.FindByEmailAsync("haldusjuht@valiteater.ee");

            if (user5 == null)
            {
                user5 = new ApplicationUser()
                {
                    UserName = "haldusjuht@valiteater.ee",
                    Email = "haldusjuht@valiteater.ee",
                    FirstName = "Kalev",
                    LastName = "Vahejuht",
                    RoleName = "Haldusjuht"

                };
                await UserManager.CreateAsync(user5, "Test!123");
                await UserManager.AddToRoleAsync(user5, "Haldusjuht");
            }




            ApplicationUser user6 = await UserManager.FindByEmailAsync("naitejuht@valiteater.ee");

            if (user6 == null)
            {
                user6 = new ApplicationUser()
                {
                    UserName = "naitejuht@valiteater.ee",
                    Email = "naitejuht@valiteater.ee",
                    FirstName = "Mia",
                    LastName = "Kadakas",
                    RoleName = "Näitejuht"

                };
                await UserManager.CreateAsync(user6, "Test!123");
                await UserManager.AddToRoleAsync(user6, "Näitejuht");
            }


            ApplicationUser user1 = await UserManager.FindByEmailAsync("anu.saar@valiteater.ee");

            if (user1 == null)
            {
                user1 = new ApplicationUser()
                {
                    UserName = "anu.saar@valiteater.ee",
                    Email = "anu.saar@valiteater.ee",
                    FirstName = "Anu",
                    LastName = "Saar",
                    ManagerId = user6.Id,
                    RoleName = "Näitleja"
                };
                await UserManager.CreateAsync(user1, "Test!123");
                await UserManager.AddToRoleAsync(user1, "Näitleja");
            }




            ApplicationUser user2 = await UserManager.FindByEmailAsync("karl.ounapuu@valiteater.ee");

            if (user2 == null)
            {
                user2 = new ApplicationUser()
                {
                    UserName = "karl.ounapuu@valiteater.ee",
                    Email = "karl.ounapuu@valiteater.ee",
                    FirstName = "Karl",
                    LastName = "Õunapuu",
                    ManagerId = user6.Id,
                    RoleName = "Näitleja"
                };
                await UserManager.CreateAsync(user2, "Test!123");
                await UserManager.AddToRoleAsync(user2, "Näitleja");
            }




            ApplicationUser user3 = await UserManager.FindByEmailAsync("andrus.lusikas@valiteater.ee");

            if (user3 == null)
            {
                user3 = new ApplicationUser()
                {
                    UserName = "andrus.lusikas@valiteater.ee",
                    Email = "andrus.lusikas@valiteater.ee",
                    FirstName = "Andrus",
                    LastName = "Lusikas",
                    ManagerId = user5.Id,
                    RoleName = "Tehnik"

                };
                await UserManager.CreateAsync(user3, "Test!123");
                await UserManager.AddToRoleAsync(user3, "Tehnik");
            }




            ApplicationUser user4 = await UserManager.FindByEmailAsync("marianne.kuusk@valiteater.ee");

            if (user4 == null)
            {
                user4 = new ApplicationUser()
                {
                    UserName = "marianne.kuusk@valiteater.ee",
                    Email = "marianne.kuusk@valiteater.ee",
                    FirstName = "Marianne",
                    LastName = "Kuusk",
                    ManagerId = user5.Id,
                    RoleName = "Tehnik"

                };
                await UserManager.CreateAsync(user4, "Test!123");
                await UserManager.AddToRoleAsync(user4, "Tehnik");
            }




        }

    }
}

